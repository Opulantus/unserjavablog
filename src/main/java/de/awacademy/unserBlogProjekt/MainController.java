package de.awacademy.unserBlogProjekt;

import de.awacademy.unserBlogProjekt.user.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
public class MainController {

    @GetMapping("/")
    public String index(@ModelAttribute("sessionUser") User sessionUser){
        return "redirect:/blogPost";
    } // Hinweis Alex: sowas mag google nicht

}
