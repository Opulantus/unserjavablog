package de.awacademy.unserBlogProjekt.blogPosts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BlogPostService {

    private BlogPostRepository blogPostRepository;

    @Autowired //hier passiert die DepIn
    public BlogPostService(BlogPostRepository blogPostRepository) {
        this.blogPostRepository = blogPostRepository;
    }

    public List<BlogPost> getBlogPostList() { //gibt Liste wieder
        return blogPostRepository.findAllByOrderByIdDesc();
    }

    public void addBlogPost (BlogPost blogPost){
        blogPostRepository.save(blogPost);
    }

}
