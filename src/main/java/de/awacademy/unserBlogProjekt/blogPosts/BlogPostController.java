package de.awacademy.unserBlogProjekt.blogPosts;

import de.awacademy.unserBlogProjekt.comments.CommentDTO;
import de.awacademy.unserBlogProjekt.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class BlogPostController {

    @Autowired
    private BlogPostService blogPostService;

    /**
     *
     * @param sessionUser geben ihn hinzu, damit wir im html darauf zugreifen können (wird im Controller Advice erstellt)
     * @param model
     * @return
     */
    @GetMapping("/blogPost")
    public String blogPost(@ModelAttribute("sessionUser") User sessionUser, /*@ModelAttribute("user") User user,*/ Model model) {
        model.addAttribute("blogPost", blogPostService.getBlogPostList());
        return "landingPage";
    }

    @GetMapping("/writeBlogPost")
    public String index (@ModelAttribute("sessionUser") User sessionUser, @ModelAttribute BlogPostDTO blogPostDTO, Model model){
        model.addAttribute("blogPostDTO", new BlogPostDTO(""));
        return "writeBlogPost";
    }

    @PostMapping("/writeBlogPost")
    public String writeBlogPost(@ModelAttribute BlogPostDTO blogPostDTO, BindingResult bindingResult, @ModelAttribute("sessionUser") User sessionUser){
        if (sessionUser == null) {
            return "redirect:/blogPost";
        }
        if(sessionUser.getAdmin() == true){
            BlogPost blogPost = new BlogPost(blogPostDTO.getTitel(), blogPostDTO.getContent(), sessionUser);
            blogPostService.addBlogPost(blogPost);
        }
        if (bindingResult.hasErrors()) {
            return "redirect:/blogPost";
        }
        return "redirect:/blogPost";
    }
}
