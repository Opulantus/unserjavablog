package de.awacademy.unserBlogProjekt.blogPosts;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

public class BlogPostDTO {

    @NotEmpty
    @Pattern(regexp = "[a-zA-Z0-9,. ]+$", message = "Darf nur Buchstaben, Satzzeichen (Komma und Punkt) und Zahlen enthalten")
    private String titel;

    @NotEmpty
    @Pattern(regexp = "[a-zA-Z0-9,. ]+$", message = "Darf nur Buchstaben, Satzzeichen (Komma und Punkt) und Zahlen enthalten")
    private String content;

    public BlogPostDTO(String content) {
        this.content = content;
    }

    public String getTitel() {
        return titel;
    }

    public String getContent(){
        return content;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
