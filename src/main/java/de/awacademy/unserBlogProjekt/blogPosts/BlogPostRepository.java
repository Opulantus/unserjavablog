package de.awacademy.unserBlogProjekt.blogPosts;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface BlogPostRepository extends CrudRepository<BlogPost, Integer> {

    List<BlogPost> findAllByOrderByIdDesc();

    Optional<BlogPost> findById(int blogPostid);
}
