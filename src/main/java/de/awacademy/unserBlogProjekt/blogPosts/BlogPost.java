package de.awacademy.unserBlogProjekt.blogPosts;

import de.awacademy.unserBlogProjekt.comments.Comment;
import de.awacademy.unserBlogProjekt.user.User;

import javax.persistence.*;
import java.text.DateFormat;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
public class BlogPost {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int id;

    private String title;
    private String content;

    @OneToMany (mappedBy = "blogPost")
    private List<Comment> comments = new ArrayList<>();

    @ManyToOne
    private User user;

    public BlogPost() {
    }

    public BlogPost(String title, String content, User user) {
        this.title = title;
        this.content = content;
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public User getUser() {return user; }
}
