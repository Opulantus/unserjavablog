package de.awacademy.unserBlogProjekt.user;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends CrudRepository<User, String> {

    boolean existsByName(String name);
    Optional<User> findByNameAndPassword(String name, String password);

    List<User> findAll();
    Optional<User> findByName(String name);
}
