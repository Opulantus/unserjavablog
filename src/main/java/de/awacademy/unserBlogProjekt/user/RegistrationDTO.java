package de.awacademy.unserBlogProjekt.user;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

public class RegistrationDTO {


    @NotEmpty              // Regular Expressions; ^ = Start of Line; $ = End of Line
    @Pattern(regexp = "^[a-zA-Z0-9]+$", message = "Darf nur Buchstaben und Zahlen enthalten.")
    private String name;

    @NotEmpty
    @Pattern(regexp = "^([a-zA-Z0-9@*#]{8,14})+$", message = "Mindestens 8 Zeichen, höchstens 14 Zeichen, Buchstaben" +
            "groß und klein, von A bis Z, Ziffern von 0 bis 9 sowie @, * und #.")
    private String password;

    public RegistrationDTO(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }
}
