package de.awacademy.unserBlogProjekt.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    private UserRepository userRepository;

    @Autowired // DepIn
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void addUser(User user) {
        userRepository.save(user);
    }

    public boolean existsByName(String name) {
        return userRepository.existsByName(name);
    }

    public Optional<User> findByNameAndPassword(String name, String password) {
        return userRepository.findByNameAndPassword(name, password);
    }

    public List<User> users (){
        return userRepository.findAll();
    }


    public User findUserById(String id){
        return userRepository.findById(id).get();
    }

    public void userToAdmin (User user){
        user.setAdmin(true);
    }


}
