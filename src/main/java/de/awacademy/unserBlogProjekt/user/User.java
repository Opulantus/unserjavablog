package de.awacademy.unserBlogProjekt.user;

import de.awacademy.unserBlogProjekt.blogPosts.BlogPost;
import de.awacademy.unserBlogProjekt.comments.Comment;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.UUID;

@Entity
public class User {

    @Id
    private String id = UUID.randomUUID().toString();

    private String name;
    private String password;

    private boolean isAdmin;

    @OneToMany (mappedBy = "user")
    private List<BlogPost> blogPostList;

    @OneToMany (mappedBy = "user")
    private List<Comment> commentList;

    // Standard-Konstr. für SQL
    public User() {
    }

    public User(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public boolean getAdmin() {
        return isAdmin;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }
}
