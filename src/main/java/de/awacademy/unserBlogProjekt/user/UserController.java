package de.awacademy.unserBlogProjekt.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/registration")
    public String entry(Model model) {
        model.addAttribute("registration", new RegistrationDTO("", ""));
        return "registration";
    }

    @PostMapping("/registration")
    public String restrationUser(@Valid @ModelAttribute("registration") RegistrationDTO registrationDTO, BindingResult bindingResult) {

        if (userService.existsByName(registrationDTO.getName())) {
            bindingResult.addError(new FieldError("registration","name", "Bereits vergeben."));
        }
        if (bindingResult.hasErrors()) {
            return "registration";
        }
        User user = new User (registrationDTO.getName(), registrationDTO.getPassword());
        userService.addUser(user);
        return "registrationSuccess";
    }

    @GetMapping("/users")
    public String users(@ModelAttribute("user") User user, Model model){
        model.addAttribute("user", userService.users());
        /*TODO: nur nicht admins können zu admins werden*/
        return "showUsers";
    }

    @GetMapping("/users/{userId}")
    public String userIsAdmin (@PathVariable String userId){
        User user = userService.findUserById(userId);
        userService.userToAdmin(user);
        userService.addUser(user);
        return "redirect:/blogPost";
    }


}
