package de.awacademy.unserBlogProjekt.comments;

import de.awacademy.unserBlogProjekt.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@Controller
public class CommentController {


    private CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @GetMapping("/comment/{blogPostId}")
    public String index (@PathVariable String blogPostId, @ModelAttribute("sessionUser") User sessionUser, Model model){
        int id = Integer.parseInt(blogPostId);
        model.addAttribute("blogPost", commentService.getBlogPost(id));
        model.addAttribute("commentDTO", new CommentDTO(""));
                return "comment";
    }

    @PostMapping("/comment/{blogPostId}")
    public String addComment(@Valid @ModelAttribute("commentDTO") CommentDTO commentDTO, BindingResult bindingResult, @ModelAttribute("sessionUser") User sessionUser, @PathVariable int blogPostId){
        if (sessionUser == null) {
            return "redirect:/blogPost"; //Kommentar wird nicht hinzugefügt. Der User kommt zurück zur Startseite
        }
        if (bindingResult.hasErrors()) {
            return "redirect:/blogPost";
        }
        Comment comment = new Comment(commentDTO.getContent(), commentService.getBlogPost(blogPostId), sessionUser);
        commentService.addComment(comment);
        return "redirect:/blogPost";
    }

}
