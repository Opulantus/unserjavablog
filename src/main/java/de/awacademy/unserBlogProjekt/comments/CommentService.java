package de.awacademy.unserBlogProjekt.comments;

import de.awacademy.unserBlogProjekt.blogPosts.BlogPost;
import de.awacademy.unserBlogProjekt.blogPosts.BlogPostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class CommentService {

    private CommentRepository commentRepository;
    private BlogPostRepository blogPostRepository;

    @Autowired
    public CommentService(CommentRepository commentRepository, BlogPostRepository blogPostRepository) {
        this.commentRepository = commentRepository;
        this.blogPostRepository = blogPostRepository;
    }

    public void addComment(Comment comment) {
        commentRepository.save(comment);
    }

    public BlogPost getBlogPost (int blogPostid) {
        return blogPostRepository.findById(blogPostid).get();
    }

    public List<Comment> commentList(){
        return commentRepository.findAllByOrderByIdAsc();
    }

}
