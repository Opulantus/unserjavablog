package de.awacademy.unserBlogProjekt.comments;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CommentRepository extends CrudRepository<Comment, Integer> {

    List<Comment> findAllByOrderByIdAsc();
}
