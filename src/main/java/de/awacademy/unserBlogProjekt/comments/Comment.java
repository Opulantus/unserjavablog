package de.awacademy.unserBlogProjekt.comments;

import de.awacademy.unserBlogProjekt.blogPosts.BlogPost;
import de.awacademy.unserBlogProjekt.user.User;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
public class Comment {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int id;

    private String content;

    @ManyToOne
    private BlogPost blogPost;

    @ManyToOne
    private User user;

    public Comment() {
    }

    public Comment(String content, BlogPost blogPost, User user) {
        this.content = content;
        this.blogPost = blogPost;
        this.user = user;
    }

    public String getContent() {
        return content;
    }

    public User getUser() {
        return user;
    }
}
