package de.awacademy.unserBlogProjekt.comments;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;


public class CommentDTO {

    @NotEmpty
    @Pattern(regexp = "[a-zA-Z0-9,. ]+$", message = "Darf nur Buchstaben, Satzzeichen (Komma und Punkt) und Zahlen enthalten")
    private String content;

    public CommentDTO(String content) {
        this.content = content;
    }

    public String getContent(){
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
