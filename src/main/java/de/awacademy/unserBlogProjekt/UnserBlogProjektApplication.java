package de.awacademy.unserBlogProjekt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UnserBlogProjektApplication {

	public static void main(String[] args) {
		SpringApplication.run(UnserBlogProjektApplication.class, args);
	}

}
