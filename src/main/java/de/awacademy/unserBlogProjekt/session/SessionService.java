package de.awacademy.unserBlogProjekt.session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Optional;

@Service
public class SessionService {

    private SessionRepository sessionRepository;

    @Autowired
    public SessionService(SessionRepository sessionRepository) {
        this.sessionRepository = sessionRepository;
    }

    public void addSession(Session session) {
        sessionRepository.save(session);
    }

    public void removeSession(Session session) {
        sessionRepository.delete(session);
    }

    public Optional<Session> findByIdAndExpiresAtAfter(String id, Instant expiresAt) {
        return sessionRepository.findByIdAndExpiresAtAfter(id, expiresAt);
    }
}
